/**
 * @name          Cv werkervaring en nevenactiviteiten form
 * @description
 * @author        Ivana Setiawan
 * @type          Molecule
 */

import { EventEmitter, Output } from "@angular/core";
import { BaseMolecule } from "core/base.molecule";
import { UxComponent } from "core/component.decorator";

@UxComponent({
  selector: "[m-cv-training-courses]",
  customTemplateUrl: "cvBlocks/cvTrainingCourses/cvTrainingCourses.html",
  host: {
    class: "edit-block",
    "[class.edit-block--closed]" : "config.views === 'completed' || config.views === 'completed-empty'",
    "[class.edit-block--empty]" : "config.views === 'empty'",
    "[class.edit-block--open]" : "config.views === 'form' || config.views === 'subform'"
  }
})

export class CvTrainingCoursesCmp extends BaseMolecule {
  public name: string = "Cv opleiding en cursussen";
  public description = "";
  public data: any;

  @Output() public opened = new EventEmitter<any>();
  @Output() public saved = new EventEmitter<any>();

  public charCount: any = {
    maxAllowed: 2000
  };

  public permittedParams: any = {
    styles: ["empty", "open", "closed"],
    title: "String",
    isRequired: "Boolean",
    isCompleted: "Boolean",
    introOpen: "String",
    introClosed: "String",
    hideSaveControls: "Boolean",
    introEmpty: "String",
    hasExtraData: "Boolean",
    views: ["empty", "form", "completed", "subform", "completed-empty"],
    openToSubform: "Boolean"
  };

  public config: any = {
    styles: "empty",
    title: "EditBlock title",
    isRequired: true,
    isCompleted: true,
    introOpen: "",
    introClosed: "",
    hideSaveControls: false,
    introEmpty: "",
    hasExtraData: false,
    views: "empty",
    openToSubform: false
  };

  public titleConfig = {
    title: "Opleiding en cursussen",
    completed: false,
    required: true
  };

  public months: string[] = [
    "Januari",
    "Februari",
    "Maart",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Augustus",
    "September",
    "October",
    "November",
    "December"
  ];

  public toggleForm: string;
  public toggleFormType: string;
  public editModeIsOn: boolean = false;
  public education: any = {};
  public course: any = {};
  public educations: any[] = [];
  public courses: any[] = [];
  public index: number = null;

  public handleClickOpen (event: Event) {
    event.preventDefault();
    this.opened.emit();
    this.config.openToSubform ? this.setView("subform") : this.setView("form");
  }

  public handleClickClose (event: Event) {
    event.preventDefault();
    if (this.toggleForm === "ja") {
      this.setView("subform");
    } else if (this.toggleForm === "nee") {
      this.setView("completed-empty");
    } else {
      this.setView("empty");
    }
  }

  public handleAddMoreItem(event: Event) {
    this.education = {};
    this.course = {};
    this.setView("form", event);
    this.editModeIsOn = false;
  }

  public handleSubmit(val: any) {
    if (this.toggleForm === "ja") {
      this.config.openToSubform = true;
      if (this.index === null) {
        if (this.toggleFormType === "education") {
          this.educations.push(val);
        } else {
          this.courses.push(val);
        }
      } else {
        if (this.toggleFormType === "education") {
          this.educations[this.index] = val;
        } else {
          this.courses[this.index] = val;
        }
      }
      this.setView("subform");
    } else {
      this.config.openToSubform = false;
      this.setView("completed-empty");
    }
    this.index = null;
    this.saved.emit();
  }

  public handleCancelForm () {
    if (!this.editModeIsOn) {
      this.education = {};
      this.course = {};
      this.setView("empty");
    } else {
      this.setView("subform");
    }
    this.editModeIsOn = false;
  }

  public handleContinueForm () {
    if (this.toggleForm === "ja") {
      this.config.openToSubform = true;
      this.setView("completed");
    } else if (this.toggleForm === "nee") {
      this.config.openToSubform = false;
      this.setView("completed-empty");
    }
  }

  public handleSave () {
    this.setView("completed-empty");
    this.saved.emit();
  }

  public handleEditItem (event: Event, item: any, i: number) {
    event.preventDefault();
    this.editModeIsOn = true;
    this.index = i;
    if (item.radioEducation === "education") {
      this.education = item.education;
      this.toggleFormType = item.radioEducation;
    } else {
      this.course = item.course;
      this.toggleFormType = item.radioEducation;
    }
    this.setView("form");
  }

  public handleDeleteItem (event: Event, item: any, i: number) {
    event.preventDefault();
    if (item.radioEducation === "education") {
      this.educations.splice(i, 1);
    } else {
      this.courses.splice(i, 1);
    }

    if (this.educations.length === 0 && this.courses.length === 0) {
      this.setView("form");
      this.toggleForm = "nee";
    }
  }
}
