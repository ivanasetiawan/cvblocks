/**
 * @name          Cv vaardigheden competenties interesses
 * @description
 * @author        Ivana Setiawan
 * @type          Molecule
 */

import { EventEmitter, Output } from "@angular/core";
import { BaseMolecule } from "core/base.molecule";
import { UxComponent } from "core/component.decorator";

@UxComponent({
  selector: "[m-cv-how-to-be-found]",
  customTemplateUrl: "cvBlocks/cvHowToBeFound/cvHowToBeFound.html",
  host: {
    class: "edit-block",
    "[class.edit-block--closed]" : "config.views === 'closed'",
    "[class.edit-block--empty]" : "config.views === 'empty'",
    "[class.edit-block--open]" : "config.views === 'open'",
    "(click)": "generalBlockClick($event)"
  }
})

export class CvHowToBeFoundCmp extends BaseMolecule {
  public name: string = "Cv hoe wilt u gevonden worden";
  public description = "";

  @Output() public opened = new EventEmitter<any>();
  @Output() public saved = new EventEmitter<any>();

  public formData: any = {
  };

  public permittedParams: any = {
    views: ["empty", "open", "closed"]
  };

  public config: any = {
    views: "empty"
  };

  public titleConfig = {
    title: "Hoe wilt u gevonden worden",
    completed: false,
    required: true
  };

  public generalBlockClick (event: Event) {
    if (this.config.views !== "empty") {
      return;
    }
    this.handleClickOpen(event);
  }

  public handleClickOpen(event: Event) {
    event.preventDefault();

    this.opened.emit();
    this.config.views = "open";
  }

  public handleClickClose(event: Event) {
    event.preventDefault();

    this.config.views = this.titleConfig.completed ? "closed" : "empty";
  }

  public handleClickSave(event: Event) {
    event.preventDefault();

    this.config.views = "closed";
    this.titleConfig.completed = true;

    this.saved.emit();
  }

}
