/**
 * @name          Cv werkervaring en nevenactiviteiten form
 * @description
 * @author        Ivana Setiawan
 * @type          Molecule
 */

import { EventEmitter, Output } from "@angular/core";
import { BaseMolecule } from "core/base.molecule";
import { UxComponent } from "core/component.decorator";

@UxComponent({
  selector: "[m-cv-experiences-activities]",
  customTemplateUrl: "cvBlocks/cvExperiencesActivities/cvExperiencesActivities.html",
  host: {
    class: "edit-block",
    "[class.edit-block--closed]" : "config.views === 'completed' || config.views === 'completed-empty'",
    "[class.edit-block--empty]" : "config.views === 'empty'",
    "[class.edit-block--open]" : "config.views === 'form' || config.views === 'subform'"
  }
})

export class CvExperiencesActivitiesCmp extends BaseMolecule {
  public name: string = "Cv werkervaring en nevenactiviteiten";
  public description = "";
  public data: any;

  @Output() public opened = new EventEmitter<any>();
  @Output() public saved = new EventEmitter<any>();

  public charCount: any = {
    maxAllowed: 2000
  };

  public permittedParams: any = {
    styles: ["empty", "open", "closed"],
    title: "String",
    isRequired: "Boolean",
    isCompleted: "Boolean",
    introOpen: "String",
    introClosed: "String",
    hideSaveControls: "Boolean",
    introEmpty: "String",
    hasExtraData: "Boolean",
    views: ["empty", "form", "completed", "subform", "completed-empty"],
    openToSubform: "Boolean"
  };

  public config: any = {
    styles: "empty",
    title: "EditBlock title",
    isRequired: true,
    isCompleted: true,
    introOpen: "",
    introClosed: "",
    hideSaveControls: false,
    introEmpty: "",
    hasExtraData: false,
    views: "empty",
    openToSubform: false
  };

  public titleConfig = {
    title: "Werkervaring en nevenactiviteiten",
    completed: false,
    required: true
  };

  public months: string[] = [
    "Januari",
    "Februari",
    "Maart",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Augustus",
    "September",
    "October",
    "November",
    "December"
  ];

  public toggleForm: string;
  public toggleFormType: string;
  public editModeIsOn: boolean = false;

  public work: any = {};
  public activity: any = {};
  public workexperiences: any[] = [];
  public activities: any[] = [];
  public index: number = null;

  public handleClickOpen (event: Event) {
    event.preventDefault();
    this.opened.emit();
    this.config.openToSubform ? this.setView("subform") : this.setView("form");
  }

  public handleEditItem (event: Event, item: any, i: number) {
    event.preventDefault();
    this.editModeIsOn = true;
    this.index = i;
    if (item.radioWorkexperience === "work") {
      this.work = item.work;
      this.toggleFormType = item.radioWorkexperience;
    } else {
      this.activity = item.activity;
      this.toggleFormType = item.radioWorkexperience;
    }
    this.setView("form");
  }

  public handleDeleteItem (event: Event, item: any, i: number) {
    event.preventDefault();
    if (item.radioWorkexperience === "work") {
      this.workexperiences.splice(i, 1);
    } else {
      this.activities.splice(i, 1);
    }

    if (this.workexperiences.length === 0 && this.activities.length === 0) {
      this.setView("form");
      this.toggleForm = "nee";
    }
  }

  public handleAddMoreItem(event: Event) {
    this.activity = {};
    this.work = {};
    this.setView("form", event);
    this.editModeIsOn = false;
  }

  public handleSubmit(val: any) {
    if (this.toggleForm === "ja") {
      this.config.openToSubform = true;
      if (this.index == null) {
        if (this.toggleFormType === "activity") {
          this.activities.push(val);
        } else {
          this.workexperiences.push(val);
        }
      } else {
        if (this.toggleFormType === "activity") {
          this.activities[this.index] = val;
        } else {
          this.workexperiences[this.index] = val;
        }
      }
      this.setView("subform");
    } else {
      this.config.openToSubform = false;
      this.setView("completed-empty");
    }
    this.index = null;
    this.saved.emit();
  }

  public handleCancelForm () {
    if (!this.editModeIsOn) {
      this.work = {};
      this.activity = {};
      this.setView("empty");
    } else {
      this.setView("subform");
    }
    this.editModeIsOn = false;
  }

  public handleSave () {
    this.setView("completed-empty");
    this.saved.emit();
  }

  public handleContinueForm () {
    if (this.toggleForm === "ja") {
      this.config.openToSubform = true;
      this.setView("completed");
    } else if (this.toggleForm === "nee") {
      this.config.openToSubform = false;
      this.setView("completed-empty");
    }
  }
}
