/**
 * @name          Cv persoonlijke gegevens
 * @description
 * @author        Ivana Setiawan
 * @type          Molecule
 */

import { EventEmitter, Output } from "@angular/core";
import { BaseMolecule } from "core/base.molecule";
import { UxComponent } from "core/component.decorator";

@UxComponent({
  selector: "[m-cv-personal-info]",
  customTemplateUrl: "cvBlocks/cvPersonalInfo/cvPersonalInfo.html",
  host: {
    class: "edit-block",
    "[class.edit-block--closed]" : "config.views === 'closed'",
    "[class.edit-block--empty]" : "config.views === 'empty'",
    "[class.edit-block--open]" : "config.views === 'open'"
  }
})

export class CvPersonalInfoCmp extends BaseMolecule {
  public name: string = "Cv persoonlijke gegevens";
  public description = "";

  @Output() public opened = new EventEmitter<any>();
  @Output() public saved = new EventEmitter<any>();

  public definitionListConfig: any = {
    styles: "table"
  };

  public formData: any = {
    firstName: "",
    initials: "",
    surnamePrefix: "",
    surname: "",
    birthDate: "",
    gender: "",
    livesInNetherlands: "",
    zipcode: "",
    houseNumber: "",
    houseNumberAddition: "",
    street: "",
    city: "",
    phoneNumber: "",
    email: "",
    emailRepeat: ""
  };

  public formDataClone: any;

  public permittedParams: any = {
    views: ["empty", "open", "closed"]
  };

  public config: any = {
    views: "empty"
  };

  public titleConfig = {
    title: "Persoonlijke gegevens",
    completed: false,
    required: true
  };

  public handleClickOpen(event: Event) {
    event.preventDefault();

    this.formDataClone = Object.assign({}, this.formData);
    this.config.views = "open";
    this.opened.emit();
  }

  public handleClickClose(event: Event) {
    event.preventDefault();

    this.formData = this.formDataClone;
    this.config.views = this.titleConfig.completed ? "closed" : "empty";
  }

  public getFullName() {
    let name = this.formData.firstName;

    name += this.formData.surnamePrefix.length ? ` ${this.formData.surnamePrefix}` : ``;
    name += this.formData.surname.length ? `${this.formData.surname}` : ``;

    return name;
  }

  public getAddress() {
    let address = this.formData.street;

    address += this.formData.houseNumber.length ? ` ${this.formData.houseNumber}` : ``;
    address += this.formData.houseNumberAddition.length ? ` ${this.formData.houseNumberAddition}` : ``;

    return address;
  }

  public getDefinitionListData() {
    return [
      { term: "Naam", description: this.getFullName() },
      { term: "Telefoonnummer", description: this.formData.phoneNumber },
      { term: "Geboortedatum", description: this.formData.birthDate },
      { term: "Geslacht", description: this.formData.gender },
      { term: "Adres", description: this.getAddress() },
      { term: "Postcode", description: this.formData.zipcode },
      { term: "Plaats", description: this.formData.city },
      { term: "E-mailadres", description: this.formData.email }
    ];
  }

  public handleClickSave(event: Event) {
    event.preventDefault();

    this.config.views = "closed";
    this.titleConfig.completed = true;
    this.definitionListConfig.items = this.getDefinitionListData();

    this.saved.emit();
  }

}
