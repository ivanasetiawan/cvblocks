/**
 * @name          Cv persoonlijke gegevens
 * @description
 * @author        Ivana Setiawan
 * @type          Molecule
 */

import { EventEmitter, Output } from "@angular/core";
import { BaseMolecule } from "core/base.molecule";
import { UxComponent } from "core/component.decorator";

@UxComponent({
  selector: "[m-cv-availability]",
  customTemplateUrl: "cvBlocks/cvAvailability/cvAvailability.html",
  host: {
    class: "edit-block",
    "[class.edit-block--closed]" : "config.views === 'closed'",
    "[class.edit-block--empty]" : "config.views === 'empty'",
    "[class.edit-block--open]" : "config.views === 'open'",
    "(click)": "generalBlockClick($event)"
  }
})

export class CvAvailability extends BaseMolecule {
  public name: string = "Cv availability";
  public description = "";

  @Output() public opened = new EventEmitter<any>();
  @Output() public saved = new EventEmitter<any>();

  public definitionListConfig: any = {
    styles: "default"
  };

  public formData: any = {
    unemployed: "",
    unemployedSince: "",
    partiallyUnemployed: "",
    hours: ""
  };

  public formDataClone: any;

  public permittedParams: any = {
    views: ["empty", "open", "closed"]
  };

  public config: any = {
    views: "empty"
  };

  public titleConfig = {
    title: "Werksituatie",
    completed: false,
    required: true
  };

  public generalBlockClick (event: Event) {
    if (this.config.views !== "empty") { return; }
    this.handleClickOpen(event);
  }

  public handleClickOpen(event: Event) {
    this.opened.emit();
    this.config.views = "open";
  }

  public handleClickClose() {
    this.config.views = this.titleConfig.completed ? "closed" : "empty";
  }

  public handleClickSave() {
    this.config.views = "closed";
    this.titleConfig.completed = true;
    this.saved.emit();
  }

}
